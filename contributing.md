# Линтеры и зависимости

1. Установить `ruff`

```pip install ruff```

2. Настроить VSCode

```{
   "notebook.formatOnSave.enabled": true,
   "notebook.codeActionsOnSave": {
       "notebook.source.fixAll": "explicit",
       "notebook.source.organizeImports": "explicit"
   },
   "[python]": {
       "editor.formatOnSave": true,
       "editor.defaultFormatter": "charliermarsh.ruff",
       "editor.codeActionsOnSave": {
           "source.fixAll": "explicit",
           "source.organizeImports": "explicit"
           }
       }
}
```


3. Установить `pre-commit`

- Установка самой библиотеки `pip install pre-commit`

- Установка конфигурации, принятой в проекте `pre-commit install`

4. Применение линтеров и форматеров. Проверка и обработка будет осуществляться автоматически при сохранении и коммите файлов. Также могут использоваться команды (при необходимости, с указанием конкретных папок файлов, которые нужно проверить/обработать):

```ruff check```

```ruff format```
